# tbd-meteor
tbd.camp - prediction and submission system

## how to
1. install [meteor](https://meteor.com)
2. clone this repo and cd into dir
3. Install npm deps with `meteor npm install`
3.5. have content in database
4. Run app with `meteor`
5. Open http://localhost:3000
6. admin:123456


content in database
-------------------

```
head -n 3 server/lib/tracks.js
Tracks = new Mongo.Collection('tracks');
Tracks.allow({
  insert: function(userId) {
```
